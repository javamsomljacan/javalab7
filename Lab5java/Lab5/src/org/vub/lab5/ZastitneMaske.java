package org.vub.lab5;

public class ZastitneMaske {
    public int kolicina;

    public ZastitneMaske(int kolicina){
        this.kolicina=kolicina;
    }

    public void ispis() {
        System.out.println("Zastitne maske: " + kolicina);
    }
}
