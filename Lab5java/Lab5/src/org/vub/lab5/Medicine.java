package org.vub.lab5;

public class Medicine {
    public String medicine;
    public int broj;

    public Medicine(String medicine, int broj){
        this.medicine=medicine;
        this.broj=broj;
    }

    public void ispis(){
        System.out.println(medicine + ": " + broj);
    }
}
