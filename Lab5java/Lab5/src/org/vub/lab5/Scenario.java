package org.vub.lab5;

public class Scenario {
    public void prvaPoslovnica(){
        var lokacija = new Location("Pliva", "Coner", "Mali Coner", "Gunduliceva 15");
        var medicine1 = new Medicine("Voltaren", 5);
        var medicine2 = new Medicine("Brufen", 10);
        var medicine3 = new Medicine("Lupocet", 11);
        var material1 = new Material("Voda", 10);
        var material2 = new Material("Prasak", 2);
        var material3 = new Material("Aloa vera", 8);
        var rukavice = new ZastitneRukavice("crne",50);
        var maske = new ZastitneMaske(50);

        lokacija.ispis();
        medicine1.ispis();
        medicine2.ispis();
        medicine3.ispis();
        material1.ispis();
        material2.ispis();
        material3.ispis();
        rukavice.ispis();
        maske.ispis();
    }

    public void drugaPoslovnica(){
        var lokacija1 = new Location("Pliva", "Coner", "Drugi Mali Coner", "Ivanovčanska 15");
        var medicine11 = new Medicine("Voltaren", 10);
        var medicine22 = new Medicine("Brufen", 65);
        var medicine33 = new Medicine("Lupocet", 7);
        var material11 = new Material("Voda", 19);
        var material22 = new Material("Prasak", 22);
        var material33 = new Material("Aloa vera", 81);
        var rukavice1 = new ZastitneRukavice("crne",100);
        var maske1 = new ZastitneMaske(150);

        lokacija1.ispis();
        medicine11.ispis();
        medicine33.ispis();
        medicine22.ispis();
        material11.ispis();
        material33.ispis();
        material22.ispis();
        maske1.ispis();
        rukavice1.ispis();
    }



    public void zatvori(){
        System.out.println("Zatvaram prvu poslovnicu i stvari se premještaju u drugu" +
                "\nStanje druge poslovnice nakon premjestaja: ");
    }

    public void drugaPoslovnicaSaPrvom(){
        var lokacija11 = new Location("Pliva", "Coner", "Drugi Mali Coner", "Ivanovčanska 15");
        var medicine111 = new Medicine("Voltaren", 10+5);
        var medicine222 = new Medicine("Brufen", 65+10);
        var medicine333 = new Medicine("Lupocet", 7+11);
        var material111 = new Material("Voda", 19+10);
        var material222 = new Material("Prasak", 22+2);
        var material333 = new Material("Aloa vera", 81+8);
        var rukavice11 = new ZastitneRukavice("crne",100+50);
        var maske11 = new ZastitneMaske(150+50);

        lokacija11.ispis();
        medicine111.ispis();
        medicine333.ispis();
        medicine222.ispis();
        material111.ispis();
        material333.ispis();
        material222.ispis();
        maske11.ispis();
        rukavice11.ispis();
    }
}
