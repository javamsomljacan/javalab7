package org.vub.lab5;

public class DrugStore extends SmallCompany {
    public String imePoslovnice;

    public DrugStore(String imeVelikeKompanije, String imeKompanije, String imePoslovnice){
        super(imeVelikeKompanije,imeKompanije);
        this.imePoslovnice=imePoslovnice;
    }
}
