package org.vub.lab7;
import java.util.*;


public class Main {

    public static void main(String[] args) {
	    //Zad1
        /*List<Integer> lista = new ArrayList<Integer>();
        lista.add(1);
        lista.add(2);
        lista.add(3);
        lista.add(4);
        lista.add(5);
        lista.remove(2);
        System.out.println(lista);*/

        //Zad2
        /*List<Integer> lista = new ArrayList<Integer>();
        List<Integer> lista2 = new ArrayList<Integer>();
        lista.add(1);
        lista.add(2);
        lista.add(3);
        lista.add(4);
        lista.add(5);

        for (int i = lista.size() - 1; i >= 0; i--) {
            lista2.add(lista.get(i));
        }

        System.out.println(lista2);*/

        //Zad3
        /*HashSet<Integer> set = new HashSet();
        set.add(1);
        set.add(2);
        set.add(3);
        set.add(4);
        set.add(5);
        System.out.println("Broj elemenata: " + set.size());*/

        //Zad4
        /*HashSet<Integer> set = new HashSet();
        set.add(1);
        set.add(2);
        set.add(3);
        set.add(4);
        set.add(5);
        System.out.println(set);
        set.remove(1);
        set.remove(2);
        set.remove(3);
        set.remove(4);
        set.remove(5);
        System.out.println(set);*/

        //Zad5
        /*TreeSet<Integer> set = new TreeSet<>();
        TreeSet<Integer> set2 = new TreeSet<>();
        set.add(1);
        set.add(2);
        set.add(3);
        set2.add(4);
        set2.add(5);
        set2.add(6);
        set2.addAll(set);
        System.out.println(set);
        System.out.println(set2);*/

        //Zad6
        /*HashMap<Integer,String> mapa = new HashMap<Integer,String>();
        mapa.put(1,"Jedan");
        mapa.put(2,"Dva");
        mapa.put(3,"Tri");
        mapa.put(4,"Cetiri");
        mapa.put(5,"Pet");
        System.out.println(mapa.entrySet());*/

        //Zad7
        /*HashMap<Integer,String> mapa = new HashMap<Integer,String>();
        mapa.put(1,"Jedan");
        mapa.put(2,"Dva");
        mapa.put(3,"Tri");
        mapa.put(4,"Cetiri");
        mapa.put(5,"Pet");

        if (mapa.containsKey(2)){
            System.out.println("Mapa sadrzi element");
        }*/

        //Zad8
        /*HashMap<Integer,String> mapa = new HashMap<Integer,String>();
        mapa.put(1,"Jedan");
        mapa.put(2,"Dva");
        mapa.put(3,"Tri");
        mapa.put(4,"Cetiri");
        mapa.put(5,"Pet");

        System.out.println(mapa.get(3));*/

        //Zad9
        /*HashMap<Integer,String> mapa = new HashMap<Integer,String>();
        mapa.put(1,"Jedan");
        mapa.put(2,"Dva");
        mapa.put(3,"Tri");
        mapa.put(4,"Cetiri");
        mapa.put(5,"Pet");

        Set set = mapa.entrySet();
        Iterator iterator = set.iterator();
        while(iterator.hasNext()) {
            Map.Entry mentry = (Map.Entry)iterator.next();
            System.out.println(mentry.getValue());
        }*/

        //Zad10
        /*TreeMap<Integer,String> mapa = new TreeMap<Integer,String>();
        mapa.put(1,"Jedan");
        mapa.put(2,"Dva");
        mapa.put(3,"Tri");
        mapa.put(4,"Cetiri");
        mapa.put(5,"Pet");
        System.out.println(mapa);
        mapa.remove(1,"Jedan");
        mapa.remove(2,"Dva");
        mapa.remove(3,"Tri");
        mapa.remove(4,"Cetiri");
        mapa.remove(5,"Pet");
        System.out.println(mapa);*/
    }
}
